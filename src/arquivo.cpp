#include "arquivo.hpp"
#include "entrada.hpp"
#include "view.hpp"

Arquivo::Arquivo(std::string diretorioCliente, std::string diretorioSocio) 
 : diretorioCliente(diretorioCliente), diretorioSocio(diretorioSocio){}

Arquivo::~Arquivo(){}

void Arquivo::save_cliente(Cliente cliente){  
    saida.open(diretorioCliente, std::fstream::app); 

    if(saida.is_open()){
        saida << cliente.get_nome() << separador << " "
              << cliente.get_cpf() << separador << " "
              << cliente.get_email() << separador << " "
              << cliente.get_telefone() << separador << " "
              << cliente.get_endereco() << separador << " "
              << cliente.get_totalDeCompras() << separador << " "
              << cliente.get_dataDeCadastro() << separador
              << std::endl;
        saida.close();
    }
}

void Arquivo::resave_cliente(std::vector<Cliente> clientes){

    saida.open(diretorioCliente);

    if(saida.is_open()){
        saida << "";
        saida.close();
        for(Cliente & cliente : clientes){
            save_cliente(cliente);
        }
    }

}

void Arquivo::alter_cliente(Cliente newCliente, Cliente oldCliente){

    std::vector<Cliente> clientes;
    clientes = list_cliente();

    unsigned long int cont = 0;
    for(Cliente & cliente : clientes){
        if(cliente == oldCliente){
            clientes[cont] = newCliente;
            resave_cliente(clientes);
            break;
        }
        cont++;
    }

}

void Arquivo::save_socio(Socio cliente){
    saida.open(diretorioSocio, std::fstream::app); 

    if(saida.is_open()){
        saida << cliente.get_nome() << separador << " "
              << cliente.get_cpf() << separador << " "
              << cliente.get_email() << separador << " "
              << cliente.get_telefone() << separador << " "
              << cliente.get_endereco() << separador << " "
              << cliente.get_totalDeCompras() << separador << " "
              << cliente.get_dataDeCadastro() << separador << " "
              << cliente.get_percentualDeGanho() << separador << " "
              << cliente.get_dataDeAssociacao() << separador
              << std::endl;
        saida.close();
    }
}

std::vector<Cliente> Arquivo::list_cliente(){

    entrada.open(diretorioCliente);
    std::vector<Cliente> clientes;
    Cliente cliente;

    if(entrada.is_open()){
        std::string item;
        std::string texto;
        unsigned long int cont = 0;
        while(entrada >> item){
            texto += item + ' ';

            if(texto.find(get_separador()) != std::string::npos){
                texto.erase(texto.length()-2);
                switch(++cont){
                    case 1:
                        cliente.set_nome(texto);
                        break;
                    case 2:
                        cliente.set_cpf(texto);
                        break;
                    case 3:
                        cliente.set_email(texto);
                        break;
                    case 4:
                        cliente.set_telefone(texto);
                        break;
                    case 5:
                        cliente.set_endereco(texto);
                        break;
                    case 6:
                        cliente.set_totalDeCompras(std::stoull(texto));
                        break;
                    case 7:
                        cliente.set_dataDeCadastro(texto);
                        clientes.push_back(cliente);                 
                        cont = 0;
                        break;  
                }
                texto = "";
            }
        }
        entrada.close();
    }

    return clientes;
}

void Arquivo::resave_socio(std::vector<Socio> clientes){
    saida.open(diretorioSocio); 
    if(saida.is_open()){
        saida << "";
        saida.close();
        for(Socio cliente : clientes){
            save_socio(cliente);
        }
    }
}

void Arquivo::alter_socio(Socio newCliente, Socio oldCliente){
    std::vector<Socio> clientes;
    clientes = list_socio();

    unsigned long int cont = 0;
    for(Socio & cliente : clientes){
        if(cliente == oldCliente){
            clientes[cont] = newCliente;
            resave_socio(clientes);
            break;
        }
        cont++;
    }
}

std::vector<Socio> Arquivo::list_socio(){
    entrada.open(diretorioSocio);
    std::vector<Socio> clientes;
    Socio cliente;

    if(entrada.is_open()){
        std::string item;
        std::string texto;
        unsigned long int cont = 0;
        while(entrada >> item){
            texto += item + ' ';

            if(texto.find(get_separador()) != std::string::npos){
                texto.erase(texto.length()-2);
                switch(++cont){
                    case 1:
                        cliente.set_nome(texto);
                        break;
                    case 2:
                        cliente.set_cpf(texto);
                        break;
                    case 3:
                        cliente.set_email(texto);
                        break;
                    case 4:
                        cliente.set_telefone(texto);
                        break;
                    case 5:
                        cliente.set_endereco(texto);
                        break;
                    case 6:
                        cliente.set_totalDeCompras(std::stoull(texto));
                        break;
                    case 7:
                        cliente.set_dataDeCadastro(texto);
                        break;
                    case 8:
                        cliente.set_dataDeAssociacao(texto);
                        break;
                    case 9:
                        cliente.set_percentualDeGanho(std::stod(texto));
                        clientes.push_back(cliente);                 
                        cont = 0;
                        break;  
                }
                texto = "";
            }
        }
        entrada.close();
    }

    return clientes;
}

std::vector<Cliente> Arquivo::find_cliente(std::string pesquisa){

    std::vector<Cliente> clientes = list_cliente();
    std::vector<Cliente> clientesEncontrados;

    bool encontrado;

    for(Cliente & cliente : clientes){

        if(cliente.get_nome().find(pesquisa) != std::string::npos){
            encontrado = true;
        }else if(cliente.get_cpf().find(pesquisa) != std::string::npos){
            encontrado = true;
        }else if(cliente.get_email().find(pesquisa) != std::string::npos){
            encontrado = true;
        }else if(cliente.get_telefone().find(pesquisa) != std::string::npos){
            encontrado = true;
        }else if(cliente.get_telefone().find(pesquisa) != std::string::npos){
            encontrado = true;
        }else if(cliente.get_endereco().find(pesquisa) != std::string::npos){
            encontrado = true;
        }else if(std::to_string(cliente.get_totalDeCompras()).find(pesquisa) != std::string::npos){
            encontrado = true;
        }else{
            encontrado = false;
        }
        if(encontrado){
            clientesEncontrados.push_back(cliente);
            encontrado = false;
        }
    }
    return clientesEncontrados;
}

std::vector<Socio> Arquivo::find_socio(std::string pesquisa){

    std::vector<Socio> clientes = list_socio();
    std::vector<Socio> clientesEncontrados;

    bool encontrado = false;
    for(Socio & cliente : clientes){

        if(cliente.get_nome().find(pesquisa) != std::string::npos){
            encontrado = true;
        }else if(cliente.get_cpf().find(pesquisa) != std::string::npos){
            encontrado = true;
        }else if(cliente.get_email().find(pesquisa) != std::string::npos){
            encontrado = true;
        }else if(cliente.get_telefone().find(pesquisa) != std::string::npos){
            encontrado = true;
        }else if(cliente.get_telefone().find(pesquisa) != std::string::npos){
            encontrado = true;
        }else if(cliente.get_endereco().find(pesquisa) != std::string::npos){
            encontrado = true;
        }else if(std::to_string(cliente.get_totalDeCompras()).find(pesquisa) != std::string::npos){
            encontrado = true;
        }else if(cliente.get_dataDeAssociacao().find(pesquisa) != std::string::npos){
            encontrado = true;
        }else if(std::to_string(cliente.get_percentualDeGanho()).find(pesquisa) != std::string::npos){
            encontrado = true;
        }else{
            encontrado = false;
        }
        if(encontrado){
            clientesEncontrados.push_back(cliente);
            encontrado = false;
        }
    }
    return clientesEncontrados;
}

void Arquivo::find_all(std::string pesquisa, Cliente * cliente, Socio * socio){

    failFindAll = false;
    
    View view;
    Entrada entrada;

    std::vector<Cliente> clientes = find_cliente(pesquisa);
    std::vector<Socio> socios = find_socio(pesquisa);

    if(clientes.size() + socios.size() == 0){
        std::cout << " *Não foi encontrado nenhum cliente" << std::endl;
        failFindAll = true;
    }else{
        long long int cont = 0;
        std::cout << std::endl;
        for(Cliente & refClientes : clientes){
            std::cout << " [" << ++cont << "] Cliente:" << std::endl;
            view.show(refClientes);
            std::cout << std::endl;
        }
        for(Cliente & refClientes : socios){
            std::cout << " [" << ++cont << "] Sócio:" << std::endl;
            view.show(refClientes);
            std::cout << std::endl;
        }
        std::cout << " [" << ++cont << "] Cancelar pesquisa" << std::endl;

        Leitura:
        cont = entrada.get_input<unsigned long int>();
        if(cont == (long long int)(clientes.size() + socios.size())+1){
            failFindAll = true;
        }else if(cont > 0 && cont <= (long long int)(clientes.size() + socios.size())){
            if(cont <= (long long int)clientes.size()){
                *cliente = clientes[cont -1];
                socio = new Socio();
            }else{
                *socio = socios[cont -clientes.size() -1];
                cliente = new Cliente;
            }
        }else{
            std::cout << " *Entrada inválida, digite novamente:" << std::endl;
            goto Leitura;
        }
    }
}

bool Arquivo::fail_FindAll(){
    return failFindAll;
}

char Arquivo::get_separador(){
    return separador;
}