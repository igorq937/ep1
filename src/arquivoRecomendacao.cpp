#include "arquivoRecomendacao.hpp"
#include "arquivoProduto.hpp"

ArquivoRecomendacao::ArquivoRecomendacao(){}

ArquivoRecomendacao::~ArquivoRecomendacao(){}

void ArquivoRecomendacao::save_compra(Produto produto){

    ArquivoProduto arqProduto(diretorio,"");
    arqProduto.save(produto);

}

std::vector<Produto> ArquivoRecomendacao::list_compra(){

    ArquivoProduto arqProduto(diretorio,"");
    return arqProduto.list();

}

std::vector<Produto> ArquivoRecomendacao::list_recomend(){

    std::vector<Produto> produtosRepetidos;
    produtosRepetidos = list_compra();
    std::vector<Produto> produtos;

    if(produtosRepetidos.size() > 0){

        Produto auxProduto;
        for(unsigned long int i = 1; i < produtosRepetidos.size(); i++){
            for(unsigned long int j = 0; j < produtosRepetidos.size() -1; j++){
                if(produtosRepetidos[i].get_nome() < produtosRepetidos[j].get_nome()){
                    auxProduto = produtosRepetidos[i];
                    produtosRepetidos[i] = produtosRepetidos[j];
                    produtosRepetidos[j] = auxProduto;
                }
            }
        }

        produtos.push_back(produtosRepetidos[0]);
        bool repetido = false;
        for(unsigned long int i = 1; i < produtosRepetidos.size(); i++){
            for(unsigned long int j = produtos.size(); j >= 1 ; j--){
                if(produtos[j-1] == produtosRepetidos[i] && i != j-1){
                    repetido = true;
                    break;
                }
            }
            if(!repetido){
                produtos.push_back(produtosRepetidos[i]);
                repetido = false;           
            }
        }

        std::vector<unsigned long int> vezes(produtos.size());
        for(unsigned long int i = 0; i < produtos.size(); i++){
            vezes[i] = 0;
            for(unsigned long int j = 0; j < produtosRepetidos.size(); j++){
                if(produtos[i] == produtosRepetidos[j]){
                    vezes[i]++;
                }
            }
        }

        unsigned long int auxVez;
        for(unsigned long int i = 1; i < vezes.size(); i++){
            for(unsigned long int j = 0; j < vezes.size() -1; j++){
                if(vezes[i] > vezes[j]){
                    auxVez = vezes[i];
                    vezes[i] = vezes[j];
                    vezes[j] = auxVez;
                    auxProduto = produtos[i];
                    produtos[i] = produtos[j];
                    produtos[j] = auxProduto;
                }
            }
        }

    }

    return produtos;

}

void ArquivoRecomendacao::set_diretorio(std::string diretorio){
    this->diretorio = diretorio;
}

char ArquivoRecomendacao::get_separador(){
    return separador;
}