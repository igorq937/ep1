#include "produto.hpp"

#include <iostream>//remove

Produto::Produto(){
    quantidade = 0;
    preco = 0.0;
}

Produto::~Produto(){}

bool Produto::operator == (Produto & produto){
    if(get_codigo() != produto.get_codigo()){
        return false;
    }else if(get_nome() != produto.get_nome()){
        return false;
    }else if(get_descricao() != produto.get_descricao()){
        return false;
    }else if(get_preco() != produto.get_preco()){
        return false;
    }else if(get_categoria() != produto.get_categoria()){
        return false;
    }else if(get_validade() != produto.get_validade()){
        return false;
    }else{
        return true;
    }
}

std::string Produto::get_codigo(){
    return codigo;
}
void Produto::set_codigo(std::string codigo){
    this->codigo = codigo;
}

std::string Produto::get_nome(){
    return nome;
}
void Produto::set_nome(std::string nome){
    this->nome = nome;
}

std::vector<std::string> Produto::get_categoria(){
    return categoria;
}
void Produto::set_categoria(std::vector<std::string> categoria){
    this->categoria = categoria;
}

std::string Produto::get_descricao(){
    return descricao;
}
void Produto::set_descricao(std::string descricao){
    this->descricao = descricao;
}

unsigned long int Produto::get_quantidade(){
    return quantidade;
}

void Produto::set_quantidade(unsigned long int quantidade){
    this->quantidade = quantidade;
}

double Produto::get_preco(){
    return preco;
}
void Produto::set_preco(double preco){
    this->preco = preco;
}

std::string Produto::get_validade(){
    return validade;
}

void Produto::set_validade(std::string validade){
    this->validade = validade;
}