#include "arquivoProduto.hpp"
#include "entrada.hpp"
#include "view.hpp"

#include <iostream>

ArquivoProduto::ArquivoProduto(std::string diretorio, std::string diretorioCategoria)
 : diretorio(diretorio), diretorioCategoria(diretorioCategoria){}
ArquivoProduto::~ArquivoProduto(){}

void ArquivoProduto::save(Produto produto){
    saida.open(diretorio, std::fstream::app);
    if(saida.is_open()){
        saida << produto.get_quantidade() << separador << " "
              << produto.get_codigo() << separador << " "
              << produto.get_nome() << separador << " "
              << produto.get_descricao() << separador << " "
              << produto.get_preco() << separador << " "
              << produto.get_validade() << separador << " "
              << produto.get_categoria().size() << separador << " ";
        for (std::string categorias : produto.get_categoria()){
            saida << categorias << separador << " ";
        }
        saida << std::endl;
        saida.close();
    }
}

void ArquivoProduto::alter(Produto newProduto, Produto oldProduto){

    std::vector<Produto> produtos;
    produtos = list();

    unsigned long int cont = 0;
    for(Produto & refProduto : produtos){
        if(refProduto == oldProduto){
            produtos[cont] = newProduto;
            resave(produtos);
            break;
        }
        cont++;
    }
}

void ArquivoProduto::resave(std::vector<Produto> produtos){
    saida.open(diretorio);
    if(saida.is_open()){
        saida << "";
        saida.close();
        for(Produto & produto : produtos){
            save(produto);
        }
    }

    
        
}

Produto ArquivoProduto::find(std::string pesquisa){
    
    std::size_t null = std::string::npos;

    std::vector<std::string> categorias;
    std::vector<Produto> produtosEncontrados;
    View view;
    Entrada entrada;

    unsigned long int cont = 0;
    bool encontrado = false;
    for(Produto & produto : list()){

        categorias.clear();
        categorias = produto.get_categoria();
        for(std::string & categoria : categorias){
            if(categoria == pesquisa)
                encontrado = true;
        }

        if(encontrado){
        }else if(produto.get_nome().find(pesquisa) != null){
            encontrado = true;
        }else if(produto.get_descricao().find(pesquisa) != null){
            encontrado = true;
        }else if(produto.get_codigo().find(pesquisa) != null){
            encontrado = true;
        }else if(std::to_string(produto.get_quantidade()).find(pesquisa) != null){
            encontrado = true;
        }else if(produto.get_validade().find(pesquisa) != null){
            encontrado = true;
        }else if(std::to_string(produto.get_preco()).find(pesquisa) != null){
            encontrado = true;
        }

        if(encontrado){
            encontrado = false;
            produtosEncontrados.push_back(produto);
            std::cout << " [" << ++cont << "]" << std::endl;
            view.show(produto);
            std::cout << std::endl;
        }
    }
    std::cout << " [" << ++cont << "] Cancelar pesquisa" << std::endl;

    if(produtosEncontrados.size() == 0){
        std::cout << " *Nada foi encontrado" << std::endl;
        failFind = true;
    }else{
        failFind = false;
    }

    Leitura:
    cont = entrada.get_input<unsigned long int>();
    if(cont == produtosEncontrados.size()+1){
        Produto produto;
        failFind = true;
        return produto;
    }else if(cont > produtosEncontrados.size()){
        std::cout << " Entrada inválida, digite novamente:" << std::endl;
        goto Leitura;
    }

    return produtosEncontrados[cont-1];
}

std::vector<Produto> ArquivoProduto::list(){

    entrada.open(diretorio);

    std::vector<Produto> produtos;

    if(entrada.is_open()){
        Produto produto;
        std::vector<std::string> categoria;
        std::string item;
        std::string texto;
        unsigned long int numeroCategorias;
        unsigned long int cont = 0;
        while(entrada >> item){
            texto += item + ' ';
            if(texto.find(get_separador()) != std::string::npos){               
                cont++;
                texto = texto.erase(texto.length()-2);
                switch(cont){
                    case 1:
                        produto.set_quantidade(std::stoul(texto));
                        break;
                    case 2:
                        produto.set_codigo(texto);
                        break;
                    case 3:
                        produto.set_nome(texto);
                        break;
                    case 4:
                        produto.set_descricao(texto);
                        break;
                    case 5:
                        produto.set_preco(std::stod(texto));
                        break;
                    case 6:
                        produto.set_validade(texto);
                        break;
                    case 7:
                        numeroCategorias = std::stoul(texto);
                        break;
                    default:
                        if(numeroCategorias > 0){
                            categoria.push_back(texto);
                            numeroCategorias -= 1;
                            if(numeroCategorias == 0){
                                produto.set_categoria(categoria);
                                produtos.push_back(produto);
                                categoria.clear();
                                cont = 0;
                            }
                        }
                        break;
                }
                texto.clear();
            }
        }

        entrada.close();
    }

    return produtos;
}

void ArquivoProduto::save_categoria(std::string categoria){

    saida.open(diretorioCategoria, std::fstream::app);
    if(saida.is_open()){
        saida << categoria << separador << std::endl;
        saida.close();
    }
}

std::vector<std::string> ArquivoProduto::list_categoria(){

    entrada.open(diretorioCategoria);
    std::vector<std::string> categorias;

    if(entrada.is_open()){
        std::string item;
        std::string texto;
        while(entrada >> item){
            texto += item + ' ';
            if(texto.find(get_separador()) != std::string::npos){
                texto = texto.erase(texto.length()-2);
                categorias.push_back(texto);
                texto = "";
            }
        }
        entrada.close();
    }

    return categorias;

}

bool ArquivoProduto::get_failFind(){
    return failFind;
}

char ArquivoProduto::get_separador(){
    return separador;
}