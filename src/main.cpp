#include "socio.hpp"
#include "arquivo.hpp"
#include "arquivoProduto.hpp"
#include "arquivoRecomendacao.hpp"
#include "produto.hpp"
#include "entrada.hpp"
#include "view.hpp"

using namespace std;

View view;
Entrada entrada;

Arquivo arquivo("dat/cliente.txt","dat/socio.txt");
ArquivoProduto arqProduto("dat/produto.txt","dat/categoria.txt");
ArquivoRecomendacao arqRecomendacao;
string diretorioRecomend = "dat/";

struct Compra{
    Produto produto;
    unsigned long int quantidade;
};

Compra compra;
vector<Compra> carrinhoDeCompras;

Cliente clienteCurrent;
Socio socioCurrent;
bool clienteIsSocio;
Produto produtoCurrent;

void modoVenda();
void modoVendaCase1();
void fazerVenda();
void showCarrinho();
void addCarrinho();
void fecharVenda();
void modoVendaCase2();

void modoRecomendacao();
void encontrarRecomendacao();

void modoEstoque();
void modoEstoque1();
void modoEstoque2();
void modoEstoque3();

int main(){

    vector<string> menuInicialE(4);
    menuInicialE[0] = "Modo venda";
    menuInicialE[1] = "Modo recomendação";
    menuInicialE[2] = "Modo estoque";
    menuInicialE[3] = "Sair";

    bool sair = false;
    while(!sair){
        system("clear");

        view.show_menu("Menu Inicial", menuInicialE);
        
        Leitura:
        switch(entrada.get_input<int>()){
            case 1:
                modoVenda();
                break;
            case 2:
                modoRecomendacao();
                break;
            case 3:
                modoEstoque();
                break;
            case 4:
                sair = true;
                break;
            default:
                cout << " *Entrada inválida, digite novamente:" << endl;
                goto Leitura;
                break;
        }
    }
    return 0;
}

void modoVenda(){

    vector<string> modoVendaE(3);
    modoVendaE[0] = "Pesquisar cliente";
    modoVendaE[1] = "Cadastrar cliente";
    modoVendaE[2] = "Voltar";

    system("clear");
    view.show_menu("Modo Venda", modoVendaE);
    Leitura:
    switch(entrada.get_input<int>()){
        case 1:
            modoVendaCase1();
            break;
        case 2:
            modoVendaCase2();
            break;
        case 3:
            break;
        default:
            cout << " *Entrada inválida, digite novamente:" << endl;
            goto Leitura;
            break;
    }
}

void modoVendaCase1(){

    Cliente cliente;
    Socio socio;

    vector<string> modoVendaE1(2);
    modoVendaE1[0] = "Pesquisar novavemnte";
    modoVendaE1[1] = "Terminar pesquisa";

    Leitura:
    system("clear");
    cout << " Digite o termo a ser pesquisado ('N' é diferente de 'n')" << endl;
    arquivo.find_all(entrada.get_input(),&cliente,&socio);
    if(arquivo.fail_FindAll()){
        std::cout << " Insira uma entrada equivalente as opções:" << std::endl;
        view.show_menu(modoVendaE1);
        switch(entrada.get_input<int>()){
            case 1:
                goto Leitura;
                break;
            case 2:
                modoVenda();              
                break;
            default:
                cout << " *Entrada inválida, digite novamente:" << endl;
                goto Leitura;
                break;
        }
    }else{
        Cliente clienteAux;
        if(cliente == clienteAux){
            socioCurrent = socio;
            clienteIsSocio = true;
        }else{
            clienteCurrent = cliente;
            clienteIsSocio = false;
        }
        fazerVenda();
    }
}

void fazerVenda(){

    system("clear");
    cout << " Cliente selecionado para venda:\n" << endl;
    
    if(clienteIsSocio){
        view.show(socioCurrent);
    }else{
        view.show(clienteCurrent);
    }

    Venda:
    showCarrinho();
    vector<string> fazerVenda(2);
    fazerVenda[0] = "Adiocionar produto ao carrinho";
    fazerVenda[1] = "Terminar venda";

    view.show_menu(fazerVenda);
    Leitura:
    switch(entrada.get_input<int>()){
        case 1:
            if(arqProduto.list().size() == 0){
                system("clear");
                cout << " *Não há produtos em estoque!" << endl;
                goto Venda;
            }
            addCarrinho();
            goto Venda;
            break;
        case 2:
            fecharVenda();
            break;
        default:
            cout << " *Entrada inválida, digite novamente:" << endl;
            goto Leitura;
            break;
    }
}

void showCarrinho(){
    unsigned long int cont;
    cout << "\n Carrinho:" << endl;
    double valorTotal = 0;
    double desconto = 0;
    for(cont = 0; cont < carrinhoDeCompras.size(); cont++){
        cout.width(50);
        cout << left << " -"+carrinhoDeCompras[cont].produto.get_nome();
        cout << carrinhoDeCompras[cont].quantidade;
        cout << right << " | R$"<< carrinhoDeCompras[cont].produto.get_preco() << endl;
        valorTotal += carrinhoDeCompras[cont].produto.get_preco();
    }
    if(carrinhoDeCompras.size() == 0){
        cout << " *Vazio" << endl;
    }
    cout << "\n Valor total:" << " R$" << valorTotal << endl;
    if(clienteIsSocio){
        desconto = valorTotal*(15/100);
        cout << " Valor do desconto: " << desconto << endl;
        desconto += valorTotal;
        cout << " Valor final: R$" << desconto << endl;
    }else{
        cout << " *Sem desconto" << endl;
    }
    cout << endl;
}

void addCarrinho(){
    bool repetido = false;
    system("clear");
    cout << "\n Digite o termo a ser pesquisado ('N' é diferente de 'n')" << endl;
    compra.produto = arqProduto.find(entrada.get_input());

    if(arqProduto.get_failFind()){

    }else{
        cout << "Quantidade:" << endl;
        Leitura:
        compra.quantidade = entrada.get_input<unsigned long int>();
        unsigned long int cont;
        unsigned long int newQuantidade;
        for(cont = 0; cont < carrinhoDeCompras.size(); cont++){
            if(carrinhoDeCompras[cont].produto.get_codigo() == compra.produto.get_codigo()){
                newQuantidade = carrinhoDeCompras[cont].quantidade + compra.quantidade;
                repetido = true;
                if(to_string(newQuantidade) == to_string(string::npos)){
                    cout << " *Entrada inválida, digite novamente:" << endl;
                    goto Leitura;
                }else{
                    carrinhoDeCompras[cont].quantidade = newQuantidade;
                }
            }
        }

        if(!repetido){
           carrinhoDeCompras.push_back(compra); 
        }
    }
    system("clear");
}

void fecharVenda(){
    unsigned long int cont;
    bool erro = false;
    for(cont = 0; cont < carrinhoDeCompras.size(); cont++){
        if(carrinhoDeCompras[cont].quantidade > carrinhoDeCompras[cont].produto.get_quantidade()){
            cout << "\n Não há estoque suficiente para realizar a compra!" << endl;
            entrada.get_input();
            erro = true;
            break;
        }
    }
    if(!erro){
        unsigned long int newQuantidade;
        Produto produtoAux;
        for(cont = 0; cont < carrinhoDeCompras.size(); cont++){
            newQuantidade = carrinhoDeCompras[cont].produto.get_quantidade() - carrinhoDeCompras[cont].quantidade;
            produtoAux = carrinhoDeCompras[cont].produto;
            produtoAux.set_quantidade(newQuantidade);
            arqProduto.alter(produtoAux, carrinhoDeCompras[cont].produto);
        }

        if(clienteIsSocio){
            Socio clienteAux = socioCurrent;
            clienteAux.set_totalDeCompras(clienteAux.get_totalDeCompras()+1);
            arquivo.alter_socio(clienteAux, socioCurrent);
            arqRecomendacao.set_diretorio(diretorioRecomend + clienteAux.get_cpf() + ".txt");
        }else{
            Cliente clienteAux = clienteCurrent;
            clienteAux.set_totalDeCompras(clienteAux.get_totalDeCompras()+1);
            arquivo.alter_cliente(clienteAux, clienteCurrent);
            arqRecomendacao.set_diretorio(diretorioRecomend + clienteAux.get_cpf() + ".txt");
        }

        std::vector<Produto> produtos;
        for(cont = 0; cont < carrinhoDeCompras.size(); cont++){
            arqRecomendacao.save_compra(carrinhoDeCompras[cont].produto);
        }

        system("clear");
        cout << " Venda terminada com sucesso!" << endl;
        entrada.get_input();
        system("clear");
    }
    carrinhoDeCompras.clear();
}

void modoVendaCase2(){

    Cliente cliente;
    Socio socio;

    vector<string> modoVendaE2(3);
    modoVendaE2[0] = "Cadastrar Cliente";
    modoVendaE2[1] = "Cadastrar Socio";
    modoVendaE2[2] = "Voltar";

    system("clear");
    std::cout << " Insira uma entrada equivalente as opções:" << std::endl;
    view.show_menu(modoVendaE2);
    Leitura:
    switch(entrada.get_input<int>()){
        case 1:
            system("clear");
            cliente = entrada.get_inputCliente();
            arquivo.save_cliente(cliente);
            cout << "\n Cliente cadastrado!" << endl;
            entrada.get_input();
            break;
        case 2:
            system("clear");
            socio = entrada.get_inputSocio();
            arquivo.save_socio(socio);
            cout << "\n Cliente cadastrado!" << endl;
            entrada.get_input();
            break;
        case 3:
            modoVenda();
            break;
        default:
            cout << " *Entrada inválida, digite novamente:" << endl;
            goto Leitura;
            break;
    }
}

void modoRecomendacao(){

    vector<string> modoRecomendacaoE(2);
    modoRecomendacaoE[0] = "Pesquisar cliente";
    modoRecomendacaoE[1] = "Voltar";

    system("clear");
    view.show_menu("Modo Recomendação", modoRecomendacaoE);
    Leitura:
    switch(entrada.get_input<int>()){
        case 1:
            encontrarRecomendacao();
            break;
        case 2:
            break;
        default:
            cout << " *Entrada inválida, digite novamente:" << endl;
            goto Leitura;
            break;
    }

}

void encontrarRecomendacao(){
    Cliente cliente;
    Socio socio;

    vector<string> findRecomend(2);
    findRecomend[0] = "Pesquisar novavemnte";
    findRecomend[1] = "Terminar pesquisa";

    Leitura:
    system("clear");
    cout << " Digite o termo a ser pesquisado ('N' é diferente de 'n')" << endl;
    arquivo.find_all(entrada.get_input(),&cliente,&socio);
    if(arquivo.fail_FindAll()){
        std::cout << " Insira uma entrada equivalente as opções:" << std::endl;
        view.show_menu(findRecomend);
        switch(entrada.get_input<int>()){
            case 1:
                goto Leitura;
                break;
            case 2:              
                break;
            default:
                cout << " *Entrada inválida, digite novamente:" << endl;
                goto Leitura;
                break;
        }
    }else{
        system("clear");
        cout << " Produtos recomendados para ";

        Cliente clienteAux;
        if(cliente == clienteAux){
            socioCurrent = socio;
            clienteIsSocio = true;
            arqRecomendacao.set_diretorio(diretorioRecomend + socio.get_cpf() + ".txt");
            cout << socio.get_nome();
        }else{
            clienteCurrent = cliente;
            clienteIsSocio = false;
            arqRecomendacao.set_diretorio(diretorioRecomend + cliente.get_cpf() + ".txt");
            cout << cliente.get_nome();
        }

        cout << ':' << endl;
        int max = 0;
        vector<Produto> produtosRecomendados = arqRecomendacao.list_recomend();
        for(Produto & produto : produtosRecomendados){
            cout << endl;
            view.show(produto);
            max++;
            if(max == 10)
                break;
        }
        cout << endl;

        if(produtosRecomendados.size() == 0){
            cout << " *Não há produtos recomendados para esse cliente" << endl;
        }

        entrada.get_input();
    }
}

void modoEstoque(){

    vector<string> modoEstoqueE(4);
    modoEstoqueE[0] = "Cadastrar produto";
    modoEstoqueE[1] = "Atualizar produto";
    modoEstoqueE[2] = "Criar categoria";
    modoEstoqueE[3] = "Voltar";

    system("clear");
    view.show_menu("Modo Estoque", modoEstoqueE);
    Leitura:
    switch(entrada.get_input<int>()){
        case 1:
            modoEstoque1();
            break;
        case 2:
            modoEstoque2();
            break;
        case 3:
            modoEstoque3();
            break;
        case 4:
            break;
        default:
            cout << " *Entrada inválida, digite novamente:" << endl;
            goto Leitura;
            break;
    }
}

void modoEstoque1(){

    Produto produto;

    produto = entrada.get_inputProduto();
    unsigned long int indice;

    selecionarCategoria:
    vector<string> categorias = arqProduto.list_categoria();
    vector<string> categoriasEscolhidas;

    if(categorias.size() > 0){

        cout << "Categoria:" << endl;
        view.show_menu(categorias);
        cout << " [" << categorias.size()+1 << "] Terminar" << endl;
        leitura:
        indice = entrada.get_input<unsigned long int>();

        if(indice > 0 && indice <= categorias.size()){

            for(string & refCategorias : categoriasEscolhidas){
                if(refCategorias == categorias[indice-1]){
                    cout << " *Essa categoria já foi escolhida" << std::endl;
                    goto leitura;
                }
            }
            categoriasEscolhidas.push_back(categorias[indice-1]);
            goto leitura;

        }else if(indice == categorias.size()+1){

            if(categoriasEscolhidas.size() == 0){
                cout << " *Não foi escolhido uma categoria" << std::endl;
                goto leitura;
            }else{
                produto.set_categoria(categoriasEscolhidas);
                arqProduto.save(produto);
                system("clear");
                cout << " Produto cadastrado com sucesso!" << endl;
                entrada.get_input();
            }

        }else{
            cout << " *Entrada inválida, digite novamente:" << endl;
            goto leitura;
        }

    }else{

        vector<string> modoEstoqueE1(2);
        modoEstoqueE1[0] = "Cadastrar categoria";
        modoEstoqueE1[1] = "Cancelar";

        std::cout << " Insira uma entrada equivalente as opções:" << std::endl;
        cout << " *Não há categorias cadastradas" << endl;
        view.show_menu(modoEstoqueE1);

        LeituraExcecao:
        switch (entrada.get_input<int>())
        {
        case 1:
            modoEstoque3();
            goto selecionarCategoria;
            break;
        case 2:
            break;   
        default:
            cout << " *Entrada inválida, digite novamente:" << endl;
            goto LeituraExcecao;
            break;
        }
    }
}

void modoEstoque2(){

    Produto produto;

    unsigned long int cont;
    unsigned long int escolha;

    vector<Produto> produtos;
    produtos = arqProduto.list();

    if(produtos.size() == 0){
        cout << " *Não há produtos cadastrados:" << endl;
        entrada.get_input();
    }else{
                
        cont = 0;
        cout << endl;
        cout << " Insira uma entrada equivalente as opções:\n" << endl;
        for(Produto & refProduto : produtos){
            cout << " [" << ++cont <<"] " << endl;
            view.show(refProduto);
            cout << endl;
        }

        Leitura:
        escolha = entrada.get_input<unsigned long int>();
        if(escolha > 0 && escolha <= cont){
            produto = produtos[escolha-1];
        }else{
            cout << " *Entrada inválida, digite novamente:" << endl;
            goto Leitura;
        }
        cout << "Quantidade:" << endl;
        Produto produtoAux = produto;
        produto.set_quantidade(entrada.get_input<unsigned long int>());
        arqProduto.alter(produto, produtoAux);
        system("clear");
        cout << " Produto alterado com sucesso!" << endl;
        entrada.get_input();
    
    }
}

void modoEstoque3(){
    system("clear");
    cout << "Nome:" << endl;
    arqProduto.save_categoria(entrada.get_input());
    system("clear");
    cout << " Categoria salva!" << endl;
    entrada.get_input();
}