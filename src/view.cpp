#include "view.hpp"

#include <iostream>
#include <vector>

View::View(){}
View::~View(){}

void View::show_menu(std::vector<std::string> escolha){
    unsigned long int cont = 0;
    for (std::string &escolhas : escolha){
        std::cout << " [" << ++cont << "]"<< " "+escolhas << std::endl;
    }
}

void View::show_menu(std::string titulo, std::vector<std::string> escolha){
    std::cout << "_______________________________" << std::endl;

    std::cout.width(30);
    std::cout << std::left << " "+titulo << std::right << "|" << std::endl;

    std::cout << "¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨¨" << std::endl;

    std::cout << " Insira uma entrada equivalente as opções:" << std::endl;
    show_menu(escolha);
}

void View::show(Cliente cliente){
    std::cout << " Nome: "<< cliente.get_nome() << std::endl;
    std::cout << " CPF: " << cliente.get_cpf() << std::endl;
    std::cout << " Email: " << cliente.get_email() << std::endl;
    std::cout << " Telefone: " << cliente.get_telefone() << std::endl;
    std::cout << " Endereço: " << cliente.get_endereco() << std::endl;
    std::cout << " Compras realizadas: " << cliente.get_totalDeCompras() << std::endl;
    std::cout << " Data de cadastro: " << cliente.get_dataDeCadastro() << std::endl;
}

void View::show(Socio cliente){
    Cliente clienteAux = cliente;
    show(clienteAux);
    std::cout << " Percentual de ganho: " << cliente.get_percentualDeGanho() << std::endl; 
    std::cout << " Data de Associação: " << cliente.get_dataDeAssociacao() << std::endl;
}

void View::show(Produto produto){
    std::cout << " Codigo: " << produto.get_codigo() << std::endl;
    std::cout << " Nome: " << produto.get_nome() << std::endl;
    std::cout << " Descrição: " << produto.get_descricao() << std::endl;
    std::cout << " Quantidade: " << produto.get_quantidade() << std::endl;
    std::cout << " Preço: " << produto.get_preco() << std::endl;
    std::cout << " Validade: " << produto.get_validade() << std::endl;
    std::cout << " Categoria(s):" << std::endl;
    for(std::string & categorias : produto.get_categoria()){
        std::cout << " -" << categorias << std::endl;
    }
}
    