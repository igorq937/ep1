#include "cliente.hpp"

Cliente::Cliente(){
    totalDeCompras = 0;  
}

Cliente::~Cliente(){}

bool Cliente::operator == (Cliente cliente){
    if(get_nome() != cliente.get_nome()){
        return false;
    }else if(get_cpf() != cliente.get_cpf()){
        return false;
    }else if(get_email() != cliente.get_email()){
        return false;
    }else if(get_telefone() != cliente.get_telefone()){
        return false;
    }else if(get_endereco() != cliente.get_endereco()){
        return false;
    }else if(get_totalDeCompras() != cliente.get_totalDeCompras()){
        return false;
    }else if(get_dataDeCadastro() != cliente.get_dataDeCadastro()){
        return false;
    }else{
        return true;
    }
}

std::string Cliente::get_nome(){
    return nome;
}
void Cliente::set_nome(std::string nome){
    this->nome = nome;
}

std::string Cliente::get_cpf(){
    return cpf;
}
void Cliente::set_cpf(std::string cpf){
    this->cpf = cpf;
}

std::string Cliente::get_email(){
    return email;
}
void Cliente::set_email(std::string email){
    this->email = email;
}

std::string Cliente::get_telefone(){
    return telefone;
}
void Cliente::set_telefone(std::string telefone){
    this->telefone = telefone;
}

std::string Cliente::get_endereco(){
    return endereco;
}
void Cliente::set_endereco(std::string endereco){
    this->endereco = endereco;
}

int Cliente::get_totalDeCompras(){
    return totalDeCompras;
}
void Cliente::set_totalDeCompras(unsigned long int totalDeCompras){
    this->totalDeCompras = totalDeCompras;
}

std::string Cliente::get_dataDeCadastro(){
    return dataDeCadastro;
}
void Cliente::set_dataDeCadastro(std::string dataDeCadastro){
    this->dataDeCadastro = dataDeCadastro;
}