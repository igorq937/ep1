#include "entrada.hpp"
#include "arquivo.hpp"

#include <string>
#include <iostream>
#include <ctime>

Entrada::Entrada(){}

Entrada::~Entrada(){}

template <typename TYPE> TYPE Entrada::get_input(){
    while(true){
        TYPE leitura;
        std::cout << "->";
        std::cin >> leitura;
        if(std::cin.fail() || std::to_string(leitura) == std::to_string(std::string::npos)){
            std::cin.clear();
            std::cin.ignore(32767,'\n');
            std::cout << " *Entrada inválida! Insira novamente: " << std::endl;
        }
        else{
            std::cin.ignore(32767,'\n');
            return leitura;
        }
    }
}

std::string Entrada::get_input(){
    while(true){
        std::cout << "->";
        std::string leitura;
        getline(std::cin, leitura);

        if(leitura.find(Arquivo::get_separador()) != std::string::npos){
            std::cout << " *Entrada inválida! Insira novamente: " << std::endl;
        }else{
            return leitura;
        }
    }
}

std::string Entrada::get_inputNotNull(){

    std::string leitura;
    Leitura:
    leitura = get_input();
    if(leitura == ""){
        std::cout << " *Entrada inválida! Insira novamente: " << std::endl;
        goto Leitura;
    }
    return leitura;
}

std::string Entrada::get_currentData(){

    time_t timeReal;
    struct tm * timeInfo;
    char buffer[10];

    time(&timeReal);
    timeInfo = localtime(&timeReal);
    strftime(buffer,10,"%d/%m/%y",timeInfo);

    return buffer;
}

Socio Entrada::get_inputSocio(){
    Socio socio;
    std::cout << "Nome:" << std::endl;
    socio.set_nome(get_inputNotNull());
    std::cout << "CPF:" << std::endl;
    socio.set_cpf(get_inputNotNull());
    std::cout << "Email:" << std::endl;
    socio.set_email(get_input());
    std::cout << "Telefone:" << std::endl;
    socio.set_telefone(get_input());
    std::cout << "Endereco" << std::endl;
    socio.set_endereco(get_input());
    socio.set_totalDeCompras(0);
    socio.set_dataDeCadastro(get_currentData());
    std::cout << "Data de Associação: " << std::endl;
    socio.set_dataDeAssociacao(get_input());
    std::cout << "Percentual de Ganhos" << std::endl;
    socio.set_percentualDeGanho(get_input<double>());
    return socio;
}

Cliente Entrada::get_inputCliente(){
    Cliente cliente;
    std::cout << "Nome:" << std::endl;
    cliente.set_nome(get_inputNotNull());
    std::cout << "CPF:" << std::endl;
    cliente.set_cpf(get_inputNotNull());
    std::cout << "Email:" << std::endl;
    cliente.set_email(get_input());
    std::cout << "Telefone:" << std::endl;
    cliente.set_telefone(get_input());
    std::cout << "Endereco" << std::endl;
    cliente.set_endereco(get_input());
    cliente.set_totalDeCompras(0);
    cliente.set_dataDeCadastro(get_currentData());

    return cliente;
}

Produto Entrada::get_inputProduto(){

    Produto produto;
    std::cout << "Quantidade:" << std::endl;
    produto.set_quantidade(get_input<unsigned long int>());
    std::cout << "Nome:" << std::endl;
    produto.set_nome(get_inputNotNull());
    std::cout << "Código:" << std::endl;
    produto.set_codigo(get_inputNotNull());
    std::cout << "Descrição:" << std::endl;
    produto.set_descricao(get_input());
    std::cout << "Preço:" << std::endl;
    produto.set_preco(get_input<double>());
    std::cout << "Validade:" << std::endl;
    produto.set_validade(get_input());
    return produto;
    
}

template int Entrada::get_input<int>();
template double Entrada::get_input<double>();
template unsigned long int Entrada::get_input<unsigned long int>();