#include "socio.hpp"

Socio::Socio(){
    percentualDeGanho = 0.0;
}

Socio::~Socio(){}

double Socio::get_percentualDeGanho(){
    return percentualDeGanho;
}
void Socio::set_percentualDeGanho(double percentualDeGanho){
    this->percentualDeGanho = percentualDeGanho;
}

std::string Socio::get_dataDeAssociacao(){
    return dataDeAssociacao;
}
void Socio::set_dataDeAssociacao(std::string dataDeAssociacao){
    this->dataDeAssociacao = dataDeAssociacao;
}