# P1 - OO 2019.2 (UnB - Gama)

### Instalação:

````
git clone https://gitlab.com/igorq937/ep1.git
````

````
make
````

Para inciar o progama:

````
make run
````

## Seleção do menu

A seleção do menu no progama e feita pela confirmação (Enter) da entrada de um número equivalente a uma opção apresentada.

## Pesquisas

Para encontrar os dados salvos no progama espera um entrada a ser pesquisada.

Em todos os casos, uma entrada vazia exibirá todos os dados cadastrados


## Modos

O progama e dividido em 3 modos:

* Modo venda
* Modo recomendação
* Modo estoque

### Modo venda

Nesta opção poderá ser feito:

* Vendas
* Cadastro de clientes

### Modo recomendação

Nesta opção poderá ser listado os produtos recomendados a um cliente.

O primeiro critério da lista é a quantidade de vezes comprada de um mesmo produto;

Como segundo critério ordem alfabética dos itens.


### Modo estoque

Nesta opção poderá ser realizado:

* Cadastro de produtos
* Atualização da quantidade de um produto
* Criação de categorias de produto
