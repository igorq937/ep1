#ifndef SOCIO_HPP
#define SOCIO_HPP

#include "cliente.hpp"

class Socio : public Cliente{

private:
    double percentualDeGanho;
    std::string dataDeAssociacao;

public:
    Socio();
    ~Socio();

    double get_percentualDeGanho();
    void set_percentualDeGanho(double oercentualDeGanho);

    std::string get_dataDeAssociacao();
    void set_dataDeAssociacao(std::string dataDeAssociacao);

};

#endif