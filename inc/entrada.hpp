#ifndef ENTRADA_HPP
#define ENTRADA_HPP

#include "cliente.hpp"
#include "socio.hpp"
#include "produto.hpp"

#include <string>

class Entrada {

public:
    Entrada();
    ~Entrada();

    template <typename TYPE> TYPE get_input();
    std::string get_input();
    std::string get_inputNotNull();

    std::string get_currentData();

    Cliente get_inputCliente();
    Socio get_inputSocio();
    Produto get_inputProduto();

};

#endif