#ifndef ARQUIVO_HPP
#define ARQUIVO_HPP

#include "cliente.hpp"
#include "socio.hpp"
#include "produto.hpp"

#include <fstream>
#include <iostream>
#include <vector>

class Arquivo {

private:
    std::ofstream saida;
    std::ifstream entrada;
    std::string diretorioCliente;
    std::string diretorioSocio; 
    bool failFindAll = false;
    static const char separador = '~';

    Arquivo();
    
public:
    Arquivo(std::string diretorioCliente, std::string diretorioSocio);
    ~Arquivo();

    void save_cliente(Cliente cliente);
    void resave_cliente(std::vector<Cliente> clientes);
    void alter_cliente(Cliente newCliente, Cliente oldCliente);
    std::vector<Cliente> find_cliente(std::string pesquisa);
    std::vector<Cliente> list_cliente();

    void save_socio(Socio cliente);
    void resave_socio(std::vector<Socio> clientes);
    void alter_socio(Socio newCliente, Socio oldCliente);
    std::vector<Socio> find_socio(std::string pesquisa);
    std::vector<Socio> list_socio();

    void find_all(std::string pesquisa, Cliente * cliente, Socio * socio);
    bool fail_FindAll();

    static char get_separador();

};

#endif