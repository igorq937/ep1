#ifndef CLIENTE_HPP
#define CLIENTE_HPP

#include <string>

class Cliente{

private:
    std::string nome;
    std::string cpf;
    std::string email;
    std::string telefone;
    std::string endereco;
    int totalDeCompras;
    std::string dataDeCadastro;

public:
    Cliente();
    ~Cliente();

    bool operator == (Cliente cliente);

    std::string get_nome();
    void set_nome(std::string nome);

    std::string get_cpf();
    void set_cpf(std::string cpf);

    std::string get_email();
    void set_email(std::string email);

    std::string get_telefone();
    void set_telefone(std::string telefone);

    std::string get_endereco();
    void set_endereco(std::string endereco);

    int get_totalDeCompras();
    void set_totalDeCompras(unsigned long int totalDeCompras);

    std::string get_dataDeCadastro();
    void set_dataDeCadastro(std::string dataDeCadastro);

};

#endif