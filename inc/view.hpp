#ifndef VIEW_HPP
#define VIEW_HPP

#include "cliente.hpp"
#include "socio.hpp"
#include "produto.hpp"

#include <vector>
#include <string>

class View{

public:
    View();
    ~View();

    void show_menu(std::vector<std::string> escolha);
    void show_menu(std::string titulo, std::vector<std::string> escolha);

    void show(Cliente cliente);
    void show(Socio cliente);
    void show(Produto produto);

};

#endif