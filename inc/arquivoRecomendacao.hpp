#ifndef ARQUIVO_RECOMENDACAO_HPP
#define ARQUIVO_RECOMENDACAO_HPP

#include "produto.hpp"

#include <string>
#include <fstream>
#include <iostream>
#include <vector>

class ArquivoRecomendacao {

private:
    std::ofstream saida;
    std::ifstream entrada;
    std::string diretorio;
    bool failFind = false;
    static const char separador = '~';

public:
    ArquivoRecomendacao();
    ~ArquivoRecomendacao();

    void save_compra(Produto produto);
    std::vector<Produto> list_compra();
    std::vector<Produto> list_recomend();

    void set_diretorio(std::string diretorio);

    static char get_separador();

};

#endif