#ifndef ARQUIVO_PRODUTO_HPP
#define ARQUIVO_PRODUTO_HPP

#include "produto.hpp"

#include <fstream>
#include <iostream>
#include <vector>

class ArquivoProduto{

private:
    std::ofstream saida;
    std::ifstream entrada;
    std::string diretorio;
    std::string diretorioCategoria;
    bool failFind = false;
    static const char separador = '~';

    ArquivoProduto();

public:
    ArquivoProduto(std::string diretorio, std::string diretorioCategoria);
    ~ArquivoProduto();

    void save(Produto produto);
    void alter(Produto newProduto, Produto oldProduto);
    void resave(std::vector<Produto> produtos);
    Produto find(std::string pesquisa);
    std::vector<Produto> list();

    void save_categoria(std::string categoria);
    std::vector<std::string> list_categoria();

    bool get_failFind();

    static char get_separador();
};

#endif