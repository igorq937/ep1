#ifndef PRODUTO_HPP
#define PRODUTO_HPP

#include <string>
#include <vector>

class Produto{

private:
    std::string codigo;
    std::string nome;
    std::vector<std::string> categoria;
    std::string descricao;
    unsigned long int quantidade;
    double preco;
    std::string validade;

public:
    Produto();
    ~Produto();

    bool operator == (Produto & produto);

    std::string get_codigo();
    void set_codigo(std::string codigo);

    std::string get_nome();
    void set_nome(std::string nome);

    std::vector<std::string> get_categoria();
    void set_categoria(std::vector<std::string> categoria);

    std::string get_descricao();
    void set_descricao(std::string descricao);

    unsigned long int get_quantidade();
    void set_quantidade(unsigned long int quantidade);

    double get_preco();
    void set_preco(double preco);

    std::string get_validade();
    void set_validade(std::string validade);
};

#endif